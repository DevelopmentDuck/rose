package main

import (
    "fmt"
    "io/ioutil"
    "io"
    "regexp"
    "os"
    "strings"
    "path/filepath"
    "flag"
    "os/exec"
    "bufio"
    "log"
    "strconv"
)

var serverCode = ""
var imports = `package main` + "\n\n" + `import (` + "\n\t" + `"fmt"` + "\n\t" + `"net/http"` + "\n\t" + `"log"` + "\n"
var sourceFiles []string
var functions = ""
var types = ""
var consolidate = 0
var watchCode = ""
var reloadCode = ""
var linebreaks = false
var sql = false
var mongodb = false
var authType = `none`
var excludeAuth []string
var port string

/* FUNCTION

Provides consolidated and consistent error checking. Pass an error and severity level in order to call.

Severity of 1 is a fatal error
Severity of 2 is a warning

*/

func check(e error, severity int) {
    if e != nil {
        switch severity {
            case 1:
                log.Fatal(e)
            case 2:
                log.Println(e)
            default:
                panic(e)
        }
    }
}

/* FUNCTION

Read each source code file in the directory indicated by the SEED file and build the website one HTTP Handler function at a time.
If other functions are found within the source code files, they are added outside of the HTTP Handler function they are found in.

Returns an error if something cannot be read, or if successful adds to a growing string which will become the compiled source code for the website.

*/

/* WHY

In order to pass the current source code directory, the function needs to be written like this and return a filepath.WalkFunc function

*/

func roseParser(roses string) filepath.WalkFunc {
    return func(source string, f os.FileInfo, err error) error {
        consolidate = 0
        /* Source is the source code file, and thus extensions such as sourceBytes and sourceString are adaptations of this original file 

        Ensure only proper ROSE files are checked

        */
        if strings.Contains(source,".rose") {
            sourceBytes, err := ioutil.ReadFile(source)
            check(err, 2)
            var formatPath = regexp.MustCompile(roses + `\\|` + roses + `/|\.rose`)
            /* DESIGN 
            
            Add source file to growing list for building the complete website source code later on

            */
            sourceFileName := formatPath.ReplaceAllString(source, "")
            sourceFiles = append(sourceFiles, sourceFileName)
            sourceString := string(sourceBytes)
            var authRegex = regexp.MustCompile(`\/!\\ignoreAuth\/!\\\r?\n`)
            if strings.Contains(sourceString, `/!\ignoreAuth/!\`) {
                excludeAuth = append(excludeAuth, sourceFileName)
                sourceString = authRegex.ReplaceAllString(sourceString, "")
            }

            var newLine = regexp.MustCompile(`\r?\n`)

            /* DESIGN

            Get imported packages in each ROSE file first and then remove them from the loaded source in order to clean up parsing Go vs HTML/JavaScript later on

            */
            var importRegex = regexp.MustCompile("(?s)--{@(.*)@}--\r?\n")
            var importPackage = importRegex.FindAllStringSubmatch(sourceString, -1)
            if len(importPackage) != 0 {
                if importPackage[0][1] != "" {
                    var importList = newLine.Split(importPackage[0][1],-1)
                    for i := 1; i < (len(importList) - 1); i++ {
                        if (strings.Contains(imports, importList[i])) {
                            log.Println("Alert: Do not need to include package " + importList[i] + " in " + source)
                        } else {
                            imports = imports + "    " + importList[i] + "\n"
                        }
                    }
                }
                sourceString = importRegex.ReplaceAllString(sourceString, "")
            }

            if linebreaks == true {
                newLine = regexp.MustCompile(``)
            }


            /* DESIGN
            
            Check for and add any functions that exist in the source code in order to clean up parsing Go vs HTML/JavaScript later on

            */
            
            var functionRegex = regexp.MustCompile(`(?sU)(func\s.*[\S\^\n]})(\r?\n{2,}|\r?\n{1,}@)`)
            var foundFunctions = functionRegex.FindAllStringSubmatch(sourceString, -1)
            for _, function := range foundFunctions {
                functions = functions + function[1] + "\n\n"
                sourceString = strings.Replace(sourceString, function[1], "", -1)
            }

            /* DESIGN
            
            Check for and add any global structs that exist in the source code in order to clean up parsing Go vs HTML/JavaScript later on

            */
            
            var structRegex = regexp.MustCompile(`(?sU)(@type\s.*[\S\^\n]}\r?\n{1,})`)
            var foundTypes = structRegex.FindAllStringSubmatch(sourceString, -1)
            for _, typeStruct := range foundTypes {
               types = types + typeStruct[1] + "\n\n"
            }
            sourceString = structRegex.ReplaceAllString(sourceString, "")

            var globalTypeRegex = regexp.MustCompile(`@type`)
            types = globalTypeRegex.ReplaceAllString(types, "type")

            var fixSubDirectories = strings.NewReplacer("/","D")

            serverCode = serverCode + "func " + fixSubDirectories.Replace(sourceFiles[len(sourceFiles)-1]) + "(w http.ResponseWriter, r *http.Request) {\n "

            sourceString = importRegex.ReplaceAllString(sourceString, "")

            //this same logic could be used to cross-compile php or asp into golang and a self-contained website executable

            //Include files first
            if strings.Contains(sourceString, "includeF(") {
                var includeFRegex = regexp.MustCompile(`<@ includeF\("(.*)"\) @>`)
                for len(includeFRegex.FindStringSubmatch(sourceString)) > 0 {
                    var foundMatch = includeFRegex.FindStringSubmatch(sourceString)
                    includeBytes, err := ioutil.ReadFile(foundMatch[1])
                    check(err, 2)
                    sourceString = strings.Replace(sourceString, foundMatch[0], string(includeBytes), 1)
                }
            }


            /* FUNCTION

            Find all embedded code and loop through it to build the consolidated source code for the server

            */
            var embeds = strings.Split(sourceString, "<@")

            var goCode string
            for i, embeddedCode := range embeds {
                goCode = ""
                if strings.Contains(embeddedCode, "@>") {
                    var codeBlock = strings.Split(embeddedCode, "@>")
                    //Check for "write"
                    if strings.Contains(codeBlock[0], "write(") {
                        var writeRegex = regexp.MustCompile(`write\((.*)\)`)
                        var soloWriteRegex = regexp.MustCompile(`\swrite\((.*)\)\s`)
                        for len(writeRegex.FindStringSubmatch(codeBlock[0])) > 0 {
                            var foundMatch = writeRegex.FindStringSubmatch(codeBlock[0])
                            if len(soloWriteRegex.ReplaceAllString(codeBlock[0], "")) == 0 {
                                codeBlock[0] = soloWriteRegex.ReplaceAllString(codeBlock[0], "")
                                consolidateCode(foundMatch[1], 0)
                            } else {
                                codeBlock[0] = strings.Replace(codeBlock[0], foundMatch[0],"fmt.Fprint(w, " + foundMatch[1] + ")", 1)
                            }
                        }
                    }

                    //Golang exists within codeBlock[0]
                    if len(codeBlock[0]) != 0 {
                        goCode = codeBlock[0]

                        if consolidate == 1 {
                            serverCode = serverCode + ")\n"
                        }
                            
                        serverCode = serverCode + "\t" + goCode + "\n"
                        consolidate = 0
                    }
                    //HTML/JS/CSS exists within codeBlock[1]
                    if codeBlock[1] != "" {
                        consolidateCode(newLine.ReplaceAllString(strings.Replace(codeBlock[1], "`", "`+\"`\"+`", -1), ""), 1)
                    } else {}
                } else {
                    if newLine.ReplaceAllString(embeddedCode, "") != "" {
                        consolidateCode(newLine.ReplaceAllString(embeddedCode, ""), 1)
                    }
                }
                if consolidate == 1 && i == (len(embeds) -1) {
                    serverCode = serverCode + ")\n"
                }
            }
            serverCode = strings.Replace(serverCode, "fmt.Fprint(w, `\r\n`)", "", -1) + "}\n\n"
        }
        return nil
    }
}

/* FUNCTION

Intelligently combines fmt.Fprint statements into one for maximum performance

Takes two arguments: the code or variable you wish to pass and an int to determine if this should be treated as HTML or Go

html = 0 is Go
html = 1 is HTML

*/

func consolidateCode(code string, html int) {
    if consolidate == 1 {
        if html == 1 {
            serverCode = serverCode + ", `" + code + "`"
        } else {
            serverCode = serverCode + ", " + code
        }
        
    } else {
        if html == 1 {
            serverCode = serverCode + "\tfmt.Fprint(w, `" + code + "`"
        } else {
            serverCode = serverCode + "\tfmt.Fprint(w, " + code
        }
        consolidate = 1
    }
}

/* FUNCTION

Mirrors output from server to compiler

*/

func copyOutput(r io.Reader) {
    scanner := bufio.NewScanner(r)
    for scanner.Scan() {
        fmt.Println(scanner.Text())
    }
}

/* FUNCTION

Handles Authentication for Each Web Page

*/

func authenticator(fileName string) string {
    var fixSubDirectories = strings.NewReplacer("/","D")
    var httpHandleFunc string
    switch authType {
        case "none":
            if fileName == "index" {
                httpHandleFunc = "\thttp.HandleFunc(\"/\", " + fileName + ")\n"
            } else {
                httpHandleFunc = "\thttp.HandleFunc(\"/"+ fileName +"/\", " + fixSubDirectories.Replace(fileName) + ")\n"
            }
        case "basic":
            if fileName == "index" {
                httpHandleFunc = "\thttp.HandleFunc(\"/\", basicAuth(index, \" + realm + \"))\n"
            } else {
                httpHandleFunc = "\thttp.HandleFunc(\"/"+ fileName +"/\", basicAuth(" + fixSubDirectories.Replace(fileName) + ", \" + realm + \"))\n"
            }
        case "basicLDAP":
            if fileName == "index" {
                httpHandleFunc = "\thttp.HandleFunc(\"/\", basicLDAPAuth(index, \" + realm + \"))\n"
            } else {
                httpHandleFunc = "\thttp.HandleFunc(\"/"+ fileName +"/\", basicLDAPAuth(" + fixSubDirectories.Replace(fileName) + ", \" + realm + \"))\n"
            }
        case "LOL":
            if fileName == "index" {
                httpHandleFunc = "\thttp.HandleFunc(\"/\", lolAuth(index, " + port + "))\n"
            } else {
                httpHandleFunc = "\thttp.HandleFunc(\"/"+ fileName +"/\", lolAuth(" + fixSubDirectories.Replace(fileName) + ", " + port + "))\n"
            }
        case "OMG":
            if fileName == "index" {
                httpHandleFunc = "\thttp.HandleFunc(\"/\", omgAuth(index))\n"
            } else {
                httpHandleFunc = "\thttp.HandleFunc(\"/"+ fileName +"/\", omgAuth(" + fixSubDirectories.Replace(fileName) + "))\n"
            }
        default:
            if fileName == "index" {
                httpHandleFunc = "\thttp.HandleFunc(\"/\", " + fileName + ")\n"
            } else {
                httpHandleFunc = "\thttp.HandleFunc(\"/"+ fileName +"/\", " + fixSubDirectories.Replace(fileName) + ")\n"
            }

    }
    return httpHandleFunc
}

func main() {
    var roses string
    var server_filename string
    flag.Parse()

    //Read in proper seed file to grow roses.
    //Config files to build the program

    files:

    config, err := os.Open(flag.Arg(0) + ".seed")
    check(err, 1)
    defer config.Close()
    configScanner := bufio.NewScanner(config)
    var configDeclarations string
    var configVars string

    //Regex to scan config files against
    var configPattern = regexp.MustCompile(`^(.*)\s=\s(.*)$`)
    for configScanner.Scan() {
        var parseConfig = configPattern.FindStringSubmatch(configScanner.Text())
        switch parseConfig[1] {
            case "port":
                port = parseConfig[2]
            case "roses":
                roses = parseConfig[2]
            case "server_filename":
                server_filename = parseConfig[2]
            case "static_dir":
                configDeclarations = configDeclarations + "var " + parseConfig[1] + " string\n"
                configVars = configVars + "\t" + parseConfig[1] + " = " + parseConfig[2] + "\n\t" + `fs := http.FileServer(http.Dir(static_dir))` + "\n\t" + `http.Handle("/" + static_dir + "/", http.StripPrefix("/" + static_dir + "/", fs))` + "\n"
            case "websocket":
                if parseConfig[2] == "true" {
                    serverCode = serverCode + "\n" + `var clients = make([]*Client, 0)
// Configure the upgrader to not care about CORS
// If this breaks over time, we can also set ReadBufferSize and WriteBufferSize to 1024 here
var upgrader = websocket.Upgrader{
    CheckOrigin: func(r *http.Request) bool {
        return true
    },
}` + "\n"
                    imports = imports + "\t" + `"github.com/gorilla/websocket"` + "\n"
                }
            case "auth":
                /* FUNCTION

                Set up desired type of authentication

                Seed must include
                    -auth (basic | basic LDAP | ldap)
                        -basic and basic LDAP require realm string
                        -basic LDAP and ldap require domain string
                            -Can be blank, just needs to exist

                Currently, locks down entire website if these options are added.

                */
                authType = parseConfig[2]
                switch parseConfig[2] {
                    case "basic":
                        functions = functions + `
                        func basicAuth(handler http.HandlerFunc, realm string) http.HandlerFunc {
                            return func(w http.ResponseWriter, r *http.Request) {
                              user, pass, ok := r.BasicAuth()
                              if !ok || len(user) == 0 || len(pass) == 0 {
                                w.Header().Set("WWW-Authenticate", "Basic realm=" + realm)
                                      w.WriteHeader(401)
                                      w.Write([]byte("Unauthorised.\n"))
                                return
                              }
                              handler(w, r)
                            }
                        }
                        `
                    case "basicLDAP":
                        imports = imports + "\t" + `"gopkg.in/ldap.v3"` + "\n\t" + `"crypto/tls"` + "\n"
                        functions = functions + `
                        func basicLDAPAuth(handler http.HandlerFunc, realm string) http.HandlerFunc {
                            return func(w http.ResponseWriter, r *http.Request) {
                              user, pass, ok := r.BasicAuth()
                              var authorized = ldapAuth(dc, domain + user, pass)
                              if !ok || len(user) == 0 || len(pass) == 0 || authorized == false {
                                w.Header().Set("WWW-Authenticate", "Basic realm=" + realm)
                                      w.WriteHeader(401)
                                      w.Write([]byte("Unauthorized.\n"))
                                return
                              }
                              handler(w, r)
                            }
                        }

                        func ldapAuth(dc string, username string, password string) bool {
                            l, err := ldap.Dial("tcp", fmt.Sprintf("%s:%d", dc, 389))
                            if err != nil {
                                log.Println(err)
                            }
                            defer l.Close()

                            // Reconnect with TLS
                            err = l.StartTLS(&tls.Config{InsecureSkipVerify: true})
                            if err != nil {
                                log.Println(err)
                            }

                            // First bind with a read only user
                            var validCredentials bool
                            err = l.Bind(username, password)
                            if err != nil {
                                validCredentials = false
                            } else {
                                validCredentials = true
                            }
                            return validCredentials
                        }
                        `
                    case "LOL":
                        // (LO)gin with (L)DAP
                        // (L)ogin (O)nce with (L)DAP
                        imports = imports + "\t" + `"github.com/gbrlsnchs/jwt/v3"` + "\n"
                        functions = functions + `
                        func addCookie(w http.ResponseWriter, name string, value string) {
                            expire := time.Now().Add(12 * time.Hour)
                            cookie := http.Cookie{
                                Name:    name,
                                Value:   value,
                                Expires: expire,
                                Path: "/",
                                HttpOnly: true,
                            }
                            http.SetCookie(w, &cookie)
                        }
                        func lolAuth(handler http.HandlerFunc, port int) http.HandlerFunc {
                            return func(w http.ResponseWriter, r *http.Request) {
                                w.Header().Set("Access-Control-Allow-Origin", "*")
                                w.Header().Set("Access-Control-Allow-Methods", "*")
                                w.Header().Set("Access-Control-Allow-Headers", "*")
                                //log.Println(w)
                                //r.URL = "LOL"
                                if len(r.Header.Get("cookie")) > 0 {
                                    r.URL.RawQuery = ""
                                    handler(w, r)
                                } else if len(r.URL.Query().Get("token")) > 0 {
                                    addCookie(w, "Token", fmt.Sprintf("%s", r.URL.Query().Get("token")))
                                    //r.URL.RawQuery = ""
                                    //http.Redirect(w, r, "http://127.0.0.1:" + strconv.Itoa(port) + r.RequestURI, 302)
                                    handler(w, r)
                                } else {
                                    http.Redirect(w, r, "http://localhost:56446/?returnTo=http://127.0.0.1:" + strconv.Itoa(port) + r.RequestURI, 302)
                                }

                                /*fmt.Println("HELLO")
                                fmt.Println(r.RemoteAddr)
                                fmt.Println(r.RequestURI)
                                fmt.Println(os.Hostname())
                                fmt.Println(port)
                                fmt.Println(r.URL.String())
                                w.Header().Set("Access-Control-Allow-Origin", "*")
                                w.Header().Set("Access-Control-Allow-Methods", "*")
                                w.Header().Set("Access-Control-Allow-Headers", "*")
                                if len(r.Header.Get("cookie")) > 7 {
                                    authorized := verifyToken(r.Header.Get("cookie")[6:len(r.Header.Get("cookie"))], secret)
                                    log.Println(r.Header.Get("cookie"))
                                    if authorized == true {
                                        handler(w, r)
                                    } else {
                                        http.Redirect(w, r, "http://localhost:25546/login/?returnTo=http://127.0.0.1:" + strconv.Itoa(port) + r.RequestURI, 302)
                                    }
                                } else {
                                    http.Redirect(w, r, "http://localhost:25546/login/?returnTo=http://127.0.0.1:" + strconv.Itoa(port) + r.RequestURI, 302)
                                }*/
                            }
                        }

                        func verifyToken(tokenToVerify string, secret string) bool {
                            type CustomPayload struct {
                                jwt.Payload
                                LoggedInAs string   "json:\"loggedInAs\""
                            }
                            now := time.Now()
                            hs256 := jwt.NewHMAC(jwt.SHA256, []byte(secret))
                            token := []byte(tokenToVerify)

                            raw, err := jwt.Parse(token) 
                            if err != nil {
                                // Handle error.
                                return false
                            }
                            if err = raw.Verify(hs256); err != nil {
                                //panic("Verification failed.")
                                // Handle error.
                                return false
                            }
                            var (
                                h jwt.Header
                                p CustomPayload
                            )
                            if h, err = raw.Decode(&p); err != nil {
                                // Handle error.
                                return false
                            }
                            if h.Algorithm != "HS256" {
                                //panic("Incorrect algorithm provided.")
                                //log.Fatal("WRONG")
                                return false
                            }

                            iatValidator := jwt.IssuedAtValidator(now)
                            expValidator := jwt.ExpirationTimeValidator(now, true)
                            audValidator := jwt.AudienceValidator(jwt.Audience{"https://golang.org", "https://jwt.io", "https://google.com", "https://reddit.com"})
                            if err := p.Validate(iatValidator, expValidator, audValidator); err != nil {
                                switch err {
                                case jwt.ErrIatValidation:
                                    // handle "iat" validation error
                                    //log.Fatal("FAILED IAT")
                                case jwt.ErrExpValidation:
                                    // handle "exp" validation error
                                    //log.Fatal("FAILED EXP")
                                case jwt.ErrAudValidation:
                                    // handle "aud" validation error
                                    //log.Fatal("FAILED AUD")
                                }
                            }

                            return true
                        }

                        func retrieveLoggedInUser(r *http.Request) string {

                            type CustomPayload struct {
                            jwt.Payload
                            LoggedInAs string   "json:\"loggedInAs\""
                        }

                        token := []byte(r.Header.Get("cookie")[6:len(r.Header.Get("cookie"))])

                        raw, err := jwt.Parse(token) 
                        if err != nil {
                            // Handle error.jh
                            panic("Unable to parse token.")
                        }
                        var p CustomPayload

                        if _, err = raw.Decode(&p); err != nil {
                            // Handle error.
                        }
                        return p.LoggedInAs
                        }
                        `

                    default:
                }
            case "line_breaks":
                /* FUNCTION 

                Retains line breaks that exist in your source files, for development or debugging purposes

                Set "line_breaks" to true if you want to retain line breaks in your files. Default is for them to be removed.
                
                */
                linebreaks, _ = strconv.ParseBool(parseConfig[2])
            case "reload_on_change":
                /* FUNCTION 

                Can set up config file to reload the server every time you save a file to the roses directory.

                Set "reload_on_change" to true for this file watcher to be included

                This attribute must be set AFTER the roses directory is set

                */
                if parseConfig[2] == "true" {
                    functions = functions + `
                    func watchDir(path string, fi os.FileInfo, err error) error {

                        // since fsnotify can watch all the files in a directory, watchers only need
                        // to be added to each nested directory
                        if fi.Mode().IsDir() {
                            return watcher.Add(path)
                        }

                        return nil
                    }
                    `
                    reloadCode = `
                    go func() {
                    // creates a new file watcher
                    watcher, _ = fsnotify.NewWatcher()
                    defer watcher.Close()

                    if err := filepath.Walk("` + roses + `", watchDir); err != nil {
                        fmt.Println("ERROR", err)
                    }

                    done := make(chan bool)

                    go func() {
                        for {
                            select {
                            case event := <-watcher.Events:
                                fmt.Printf("EVENT! %#v\n", event)
                                os.Exit(0)
                            case err := <-watcher.Errors:
                                fmt.Println("ERROR", err)
                            }
                        }
                    }()

                    <-done}()
                    `
                    serverCode = serverCode + "\n" + `var watcher *fsnotify.Watcher` + "\n"
                    imports = imports + "\t" + `"os"` + "\n\t" + `"path/filepath"` + "\n\t" + `"github.com/fsnotify/fsnotify"` + "\n"
                }
            case "sql":
                /* FUNCTION

                Sets up ability to use built-in easy SQL query functions

                Adds the function queryDB to your sever, which requires:
                    -Driver (mssql, mysql, etc)
                    -dsn (server=SQLSERVER;user id=username;password=pass;database=DATA)
                    -cmd (SQL code)

                Returns a map of string slices which get used like this:

                results = queryDB(...)

                log.Println(results["column name"][row number])

                */
                if parseConfig[2] == "true" {
                    imports = imports + "\t\"database/sql\"\n\t\"time\"\n\t\"strconv\"\n"
                    functions = functions + `
                    func queryDB(driver string, dsn string, cmd string) map[string][]string {
                        results := make(map[string][]string)
                        var db *sql.DB
                        db, err := sql.Open(driver, dsn)
                        if err != nil {
                            log.Println("Error opening database connection", err)
                        }
                        err = db.Ping()
                        if err != nil {
                            log.Println("Error connecting to the database", err)
                        }
                        rows, err := db.Query(cmd)
                        if err != nil {
                            log.Println("Error returning rows", err)
                        }
                        defer rows.Close()
                        cols, err := rows.Columns()
                        if err != nil {
                          log.Println("Error retrieving columns", err)
                        }
                        for rows.Next() {
                            columns := make([]interface{}, len(cols))
                            columnPointers := make([]interface{}, len(cols))
                            for i, _ := range columns {
                                columnPointers[i] = &columns[i]
                            }
                            
                            err := rows.Scan(columnPointers...)
                            if err != nil {
                                log.Println(err)
                            }

                            for i, colName := range cols {
                                val := columnPointers[i].(*interface{})
                                //convert everything to strings
                                strungVariable := ""
                                switch v := (*val).(type) {
                                    case nil:
                                        strungVariable = "NULL"
                                    case bool:
                                        if v {
                                            strungVariable = "true"
                                        } else {
                                            strungVariable = "false"
                                        }
                                    case []byte:
                                        strungVariable = string(v)
                                    case time.Time:
                                        strungVariable = (v.Format("2006-01-02 15:04:05"))
                                    case float64:
                                        if v == float64(1.79e308) {
                                            strungVariable = ""
                                        } else if v == float64(-1.79e308) {
                                            strungVariable = "0"
                                        } else {
                                            strungVariable = strconv.FormatFloat(v, 'f', -1, 64)
                                        }
                                    case int64:
                                        strungVariable = strconv.Itoa(int(v))
                                    default:
                                        strungVariable = v.(string)
                                }
                                results[colName] = append(results[colName], strungVariable)
                            }
                        }
                        err = rows.Err()
                        if err != nil {
                            log.Println("Error returning rows", err)
                        }
                        db.Close()
                        db = nil
                        return results
                    }
                    func commandDB(driver string, dsn string, cmd string) {
                        var db *sql.DB
                        db, err := sql.Open(driver, dsn)
                        if err != nil {
                            log.Println("Error opening database connection", err)
                        }
                        err = db.Ping()
                        if err != nil {
                            log.Println("Error connecting to the database", err)
                        }
                        result, err := db.Exec(cmd)
                        if err != nil {
                            log.Println("Error executing command", err)
                        }
                        rows, err := result.RowsAffected()
                        if err != nil {
                            log.Println("Error executing command")
                        }
                        if rows != 1 {
                            //log.Println("Command did not affect any rows")
                        }
                        db.Close()
                        db = nil
                    }
                    `
                }

            case "sql_package":
                /* FUNCTION

                Specify what SQL package you want

                */
                imports = imports + "\t" + `_ "` + parseConfig[2] + `"` + "\n"

            case "mongodb":
                /* FUNCTION

                Automatically imports MongoDB driver set

                */
                if parseConfig[2] == "true" {
                    imports = imports + "\t\"go.mongodb.org/mongo-driver/bson\"\n\t\"go.mongodb.org/mongo-driver/mongo\"\n\t\"go.mongodb.org/mongo-driver/mongo/options\"\n\t\"context\"\n"
                }
                
            default:
                configDeclarations = configDeclarations + "var " + parseConfig[1] + " string\n"
                configVars = configVars + "\t" + parseConfig[1] + " = " + parseConfig[2] + "\n"

        }
    }

    check(configScanner.Err(), 1)
    err = filepath.Walk(roses, roseParser(roses))
    check(err, 1)

    f, err := os.Create(server_filename + ".go")
    check(err, 1)
    defer f.Close()
    //var fixSubDirectories = strings.NewReplacer("/","D")
    serverCode = serverCode + `func faviconHandler(w http.ResponseWriter, r *http.Request) {} 
    func main() {` + "\n" + configVars + "\t" + `log.Println("Starting Server")
    log.Println("http://localhost:` + port + "\")\n"
    for _, fileName := range sourceFiles {
        var excluded = false
        for _, excludedAuth := range excludeAuth {
            if fileName == excludedAuth {
                excluded = true
                var actualAuthType = authType
                authType = "none"
                serverCode = serverCode + authenticator(fileName)
                authType = actualAuthType
            }
        }
        if excluded == false {
            serverCode = serverCode + authenticator(fileName)
        }
    }
    _, err = f.WriteString(imports + ")\n\n" + configDeclarations + "\n" + functions + types + serverCode + "\thttp.HandleFunc(\"/favicon.ico\", faviconHandler) \n" + reloadCode + "\thttp.ListenAndServe(\":" + port + "\", nil)\n}")

    check(err, 1)

    f.Sync()

    cmd := exec.Command("go", "run", server_filename + ".go")
    stdout, err := cmd.StdoutPipe()
    check(err, 1)
    stderr, err := cmd.StderrPipe()
    check(err, 1)
    err = cmd.Start()
    check(err, 1)
    go copyOutput(stdout)
    go copyOutput(stderr)
    cmd.Wait()
    
    if len(reloadCode) > 0 {
        log.Println("RELOADING")
        stdout.Close()
        stderr.Close()
        /* WHY
        
        Reset all variables in Rose in order to get a truly fresh reload and recompile of the website, being able to include configuration file changes

        */
        serverCode = ""
        imports = `package main` + "\n\n" + `import (` + "\n\t" + `"fmt"` + "\n\t" + `"net/http"` + "\n\t" + `"log"` + "\n"
        sourceFiles = []string{}
        functions = ""
        types = ""
        consolidate = 0
        watchCode = ""
        reloadCode = ""
        port = ""
        roses = ""
        server_filename = ""
        linebreaks = false
        sql = false
        authType = "none"
        excludeAuth = []string{}
            goto files
    }
    
}