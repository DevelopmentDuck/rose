package main

import (
	"fmt"
	"net/http"
	"log"
	"os"
	"path/filepath"
	"github.com/fsnotify/fsnotify"
    "strings"
    "strconv"
)

var static_dir string


                    func watchDir(path string, fi os.FileInfo, err error) error {

                        // since fsnotify can watch all the files in a directory, watchers only need
                        // to be added to each nested directory
                        if fi.Mode().IsDir() {
                            return watcher.Add(path)
                        }

                        return nil
                    }
                    func exampleFunction(simpleArgument string) string {
    return strings.Replace(simpleArgument, "World", "Pluto", -1)
}



var watcher *fsnotify.Watcher
func index(w http.ResponseWriter, r *http.Request) {
 	fmt.Fprint(w, `
<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Simple Example</title>
    </head>
    <body>
        Hello, `, exampleFunction("World"), `. Welcome to Rose!<br />
        <img src="`, static_dir, `/logo.png">
        <br /<br />
        Look at how easy it is to count to 20:<br />
        `)
	 for i := 0; i < 21; i++ {
            fmt.Fprint(w, strconv.Itoa(i) + "<br />")
        } 
	fmt.Fprint(w, `
    </body> 
</html>

`)
	

}

func faviconHandler(w http.ResponseWriter, r *http.Request) {} 
    func main() {
	static_dir = `static`
	fs := http.FileServer(http.Dir(static_dir))
	http.Handle("/" + static_dir + "/", http.StripPrefix("/" + static_dir + "/", fs))
	log.Println("Starting Server")
    log.Println("http://localhost:7673")
	http.HandleFunc("/", index)
	http.HandleFunc("/favicon.ico", faviconHandler) 

                    go func() {
                    // creates a new file watcher
                    watcher, _ = fsnotify.NewWatcher()
                    defer watcher.Close()

                    if err := filepath.Walk("roses", watchDir); err != nil {
                        fmt.Println("ERROR", err)
                    }

                    done := make(chan bool)

                    go func() {
                        for {
                            select {
                            case event := <-watcher.Events:
                                fmt.Printf("EVENT! %#v\n", event)
                                os.Exit(0)
                            case err := <-watcher.Errors:
                                fmt.Println("ERROR", err)
                            }
                        }
                    }()

                    <-done}()
                    	http.ListenAndServe(":7673", nil)
}